/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.diagramxo;

import java.util.Scanner;

/**
 *
 * @author armme
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('o');
        this.player2 = new Player('x');
    }
    
    public void newGame() {
        this.table = new Table(player1, player2);
    }
    
    public void play(){
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()) {
                showTable();
                showWin();
                showInfo();
                newGame();
                table.switchPlayer();
            }
            if(table.checkDraw()) {
                showTable();
                showWin();
                showInfo();
                newGame();
                table.switchPlayer();
            }
            table.switchPlayer();
        }
    }
    
    
    private void showWelcome() {
        System.out.println("Welcome to XO Game !!!!!");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    
    private void showWin() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Winnnnn!!!!!!!!");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input Row and Col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row,col);
    }
    
    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
